import React, { Component } from "react";
import { Routes, Route } from "react-router-dom";
// components
import {
  NavbarComponent,
  HeaderComponent,
  BgBottomComponent,
  FooterComponent
} from "./components";

// pages
import { Home, Contact } from "./pages";

import { Container } from "react-bootstrap";

export default class App extends Component {
  render() {
    return (
      <div>
        {/* Navbar */}
        <NavbarComponent />

        {/* Header */}
        <HeaderComponent />

        {/* Route */}
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/contact" element={<Contact />} />
          </Routes>
        </Container>

        {/* Background Bottom */}
        <BgBottomComponent />

        {/* Footer */}
        <FooterComponent />
      </div>
    );
  }
}
