import MyPhoto from "./me.png";

// icon keahlian
import IcHTML from "./html.svg";
import IcCSS from "./css.svg";
import IcBootstrap from "./bootstrap.svg";
import IcLaravel from "./laravel.svg";
import IcReact from "./react.svg";
import IcFigma from "./figma.svg";
import IcJavascript from "./javascript.svg";
import IcPHP from "./php.svg";
import IcCodeigniter from "./codeigniter.svg";
import IcFirebase from "./firebase.svg";
import IcMySql from "./mysql.svg";
import IcSass from "./sass.svg";

// icon footer
import IcFire from "./fire.svg";

export {
  MyPhoto,
  IcHTML,
  IcCSS,
  IcBootstrap,
  IcLaravel,
  IcReact,
  IcFigma,
  IcJavascript,
  IcPHP,
  IcCodeigniter,
  IcFirebase,
  IcMySql,
  IcSass,
  IcFire
};
