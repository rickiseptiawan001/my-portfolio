import React, { Component } from 'react';
import { IcFire } from '../../assets';

export default class FooterComponent extends Component {
  render() {
    return (
      <div className="footer">
        <p className="p-footer">
          &copy; All Right Reserved Ricki Septiawan
          <img src={IcFire} alt="icon-fire" className="icon-fire" />
        </p>
      </div>
    )
  }
}
