import NavbarComponent from "./NavbarComponent";
import HeaderComponent from "./HeaderComponent";
import BgBottomComponent from "./BgBottomComponent";
import FooterComponent from "./FooterComponent";

export { NavbarComponent, HeaderComponent, BgBottomComponent, FooterComponent };
