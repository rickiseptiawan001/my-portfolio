import React from 'react';
import App from './App';

import * as ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

// import ini agar library bootstrap bisa dipakai
import 'bootstrap/dist/css/bootstrap.min.css'

// my styling
import '../src/assets/style/main.css'

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById('root')
);
