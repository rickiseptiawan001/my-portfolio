import React, { Component } from "react";
import { Card } from "react-bootstrap";

export default class Contact extends Component {
  render() {
    return (
      <div className="contact-page">
        <h1 className="heading text-white">Contact</h1>
        <Card className="card-content">
          <p className="paragraf">
            <span className="bold">WhatsApp :</span>
          </p>
          <a href="https://wa.me/628558293996" className="btn btn-wa mb-3">082210242869</a>

          <p className="paragraf">
            <span className="bold">Email :</span>
            <br />
            rickiseptiawan001@gmail.com.
          </p>

          <p className="paragraf">
            <span className="bold">Number Phone :</span>
            <br />
            082210242869
          </p>

          <p className="paragraf">
            <span className="bold">Address :</span>
            <br />
            Tangerang (Indonesia).
          </p>

          <p className="paragraf">
            <span className="bold">Note :</span>
            <br />
            If you have any question, dont worry just contact me! thanks homies
          </p>
        </Card>
      </div >
    )
  }
}