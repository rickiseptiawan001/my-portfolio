import React, { Component } from "react";

import { Card } from "react-bootstrap";

import {
  MyPhoto,
  IcHTML,
  IcCSS,
  IcBootstrap,
  IcLaravel,
  IcReact,
  IcFigma,
  IcJavascript,
  IcPHP,
  IcCodeigniter,
  IcFirebase,
  IcMySql,
  IcSass
} from "../../assets";

// animate
import AOS from 'aos';
import 'aos/dist/aos.css';
// run animate
AOS.init();

export default class Home extends Component {
  
  render() {
    return (
      <div>
        <div className="foto text-center">
          <img src={MyPhoto} alt="foto-ricki" className="my-photo" />
          <h1 className="bold-name mt-2 typed">Ricki Septiawan S.Kom</h1>
          <h1 className="bold-skill mt-2">
            Front End, Back End, Mobile Developer, UI dan UX Desainer
          </h1>
        </div>
        <Card className="card-content">
          <Card.Body>
            {/* Tentang Saya */}
            <h1 className="heading" data-aos="fade-up">About Me</h1>
            <p className="paragraf" data-aos="fade-up">
              Lemme introduce my self, i'm Ricki Septiawan (22 Years Old), i'm a fullstack developer, I Have 2 Years Experience coding at companies and as freelancer.
            </p>

            <p className="paragraf" data-aos="fade-up">
              My Hobby : Music, Anime, Game, Learn Something New
              <br />
              I like challenge, like build application from zero to hero thats why i like coding.
            </p>

            <div className="row">
              {/* Pendidikan */}
              <div className="col-lg-6">
                <h1 className="heading" data-aos="fade-up">Education</h1>
                <p className="paragraf" data-aos="fade-up">
                  <span className="bold">*Elementary School</span>
                  <br />
                  SDN Cukangalih 1, Tangerang (Indonesia)
                </p>

                <p className="paragraf" data-aos="fade-up">
                  <span className="bold">*Middle School</span>
                  <br />
                  MTS AL-Hikmah Tangerang (Indonesia)
                </p>

                <p className="paragraf" data-aos="fade-up">
                  <span className="bold">*High School</span>
                  <br />
                  SMK AL-Hikmah Tangerang (Indonesia)
                  <br />
                  Major : Internet Networking
                </p>

                <p className="paragraf" data-aos="fade-up">
                  <span className="bold">*University</span>
                  <br />
                  Global Institute, Kec. Karawaci Tangerang (Indonesia)
                  <br />
                  Major : IT - Software Engineering
                </p>
              </div>

              {/* pengalaman */}
              <div className="col-lg-6">
                <h1 className="heading" data-aos="fade-up">Experiences</h1>
                <p className="paragraf" data-aos="fade-up">
                  <span className="bold">*PT. Data Sinergitama Jaya</span>
                  <br />
                  Position : IT Support (3 Months Intership)
                </p>

                <p className="paragraf" data-aos="fade-up">
                  <span className="bold">*Glogal Institute</span>
                  <br />
                  Position : Programmer Mobile & Programmer Website (1 Year)
                </p>

                <p className="paragraf" data-aos="fade-up">
                  <span className="bold">*PT. Satria Digital Sejahtera</span>
                  <br />
                  Position : Programmer Mobile & Programmer Website (Maret 2023 - Now)
                </p>
              </div>
            </div>

            {/* keahlian programming */}
            <h1 className="heading text-center" data-aos="fade-up">Programming Skills</h1>
            <div className="row text-center icon-keahlian-part1">
              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcHTML} alt="icon-html" className="mb-3" />
                <p className="paragraf">HTML</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcCSS} alt="icon-css" className="mb-3" />
                <p className="paragraf">CSS</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcBootstrap} alt="icon-bootstrap" className="mb-3" />
                <p className="paragraf">Bootstrap</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcLaravel} alt="icon-laravel" className="mb-3" />
                <p className="paragraf">Laravel</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcReact} alt="icon-react" className="mb-3" />
                <p className="paragraf">React JS, React Native</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcFigma} alt="icon-figma" className="mb-3" />
                <p className="paragraf">Figma</p>
              </div>
            </div>

            <div className="row text-center icon-keahlian-part2">
              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcJavascript} alt="icon-javascript" className="mb-3" />
                <p className="paragraf">Javascript</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcPHP} alt="icon-php" className="mb-3" />
                <p className="paragraf">PHP</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcCodeigniter} alt="icon-codeigniter" className="mb-3" />
                <p className="paragraf">Codeigniter</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcFirebase} alt="icon-firebase" className="mb-3" />
                <p className="paragraf">Firebase</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcMySql} alt="icon-mysql" className="mb-3" />
                <p className="paragraf">MySQL</p>
              </div>

              <div className="col-6 col-lg-2" data-aos="fade-up">
                <img src={IcSass} alt="icon-sass" className="mb-3" />
                <p className="paragraf">Sass</p>
              </div>
            </div>
          </Card.Body>
        </Card>
      </div>
    );
  }
}
